FROM python:3.8

ENV FLASK_APP media_url_update_service_app

EXPOSE 5000
ENTRYPOINT ["gunicorn"]
CMD [ "wsgi:app", "--config", "/configs/gunicorn.conf.py"]

WORKDIR /
ADD setup.py /
RUN mkdir media_url_update_service_app \
  && pip install -e .

WORKDIR /media_url_update_service_app
ADD media_url_update_service_app .
