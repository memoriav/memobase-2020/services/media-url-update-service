## Media URL Update Service

This service provides an endpoint to make updates to the URLs of remote media files in the media database.

[Confluence Documentation](https://ub-basel.atlassian.net/wiki/spaces/MEMOBASE/pages/1906606081/Service+Media+URL+Updater)