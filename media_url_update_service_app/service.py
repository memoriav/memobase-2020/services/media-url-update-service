#  media-url-update-service
#  Copyright (C) 2020  Memoriav
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published
#  by the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import mysql
import mysql.connector as mariadb
from flasgger import swag_from
from flask import request
from flask.views import MethodView

from media_url_update_service_app.app import app


class MediaUrlUpdateService(MethodView):

    def __init__(self):
        try:
            self.mariadb_connection = mariadb.connect(
                user=app.config["mariadb-user"],
                password=app.config["mariadb-password"],
                host=app.config["mariadb-host"],
                port=app.config["mariadb-port"],
                database=app.config["mariadb-database"],
                ssl_ca=app.config["mariadb-host-certs"],
                ssl_verify_cert=True,
            )
            self.mariadb_connection.autocommit = False
            app.logger.debug("connecting to database")
            self.db_cursor = self.mariadb_connection.cursor()
        except mysql.connector.Error as ex:
            self.message = ex.msg
            app.logger.error(self.message)
            self.db_cursor = None
        except Exception as ex:
            self.message = f"Exception: {ex}."
            app.logger.error(self.message)
            self.db_cursor = None

    @swag_from("swagger.yml")
    def post(self):
        if self.db_cursor is None:
            return {"status": "FAILURE", "message": self.message}, 500
        records_to_update = request.get_json(force=True)
        return self._update_records_in_database(records_to_update)

    def _update_records_in_database(self, records_to_update):
        for record in records_to_update:
            try:
                sql_stmt = (
                    "UPDATE entities "
                    + 'SET uri = "'
                    + record["uri"]
                    + '" '
                    + ", lastchange = NOW() "
                    + ", notifiedon = NULL "
                    + 'WHERE sig = "'
                    + record["sig"]
                    + '"'
                )
                app.logger.debug("executing: " + sql_stmt)
                self.db_cursor.execute(sql_stmt)
                sql_stmt = (
                    "UPDATE metadata "
                    + "SET modificationtime = NOW() "
                    + 'WHERE sig = "'
                    + record["sig"]
                    + '"'
                )
                app.logger.debug("executing: " + sql_stmt)
                self.db_cursor.execute(sql_stmt)
            except mysql.connector.Error as ex:
                msg = ex.msg
                app.logger.error(msg)
                return {"status": "FATAL", "message": msg}
            except Exception as ex:
                msg = f"Exception: {ex}"
                app.logger.error(msg)
                return {"status": "FATAL", "message": msg}
        app.logger.debug("committing sql")
        self.mariadb_connection.commit()
        return {"status": "SUCCESS"}, 200
