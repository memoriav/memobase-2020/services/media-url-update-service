# Import API Service
# Copyright (C) 2020-2021 Memobase Project
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import logging
import os

from flasgger import Swagger
from flask import send_from_directory, redirect
from flask.logging import default_handler

from media_url_update_service_app.app import app
from media_url_update_service_app.service import MediaUrlUpdateService

# If app is started via gunicorn
if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)
    app.logger.removeHandler(default_handler)
    app.logger.info('Starting production server')

try:
    app.config['mariadb-host'] = os.environ['MARIADB_HOST']
    app.config['mariadb-database'] = os.environ['MARIADB_DATABASE']
    app.config['mariadb-port'] = os.environ['MARIADB_PORT']
    app.config['mariadb-user'] = os.environ['MARIADB_USER']
    app.config['mariadb-password'] = os.environ['MARIADB_PASSWORD']
    app.config['mariadb-host-certs'] = os.environ['MARIADB_HOST_CERTS']
    app.config['namespace'] = os.environ['NAMESPACE']

    app.config['SWAGGER'] = {
        'title': 'Memobase Sitemap API',
        'version': 'dev',
        'uiversion': 3,
        'termsOfService': 'http://memobase.ch/de/disclaimer',
        'description': 'And endpoint to update the sitemap of the Memobase platform.',
        'contact': {
            'name': 'UB Basel',
            'url': 'https://ub.unibas.ch',
            'email': 'memobase@unibas.ch'},
        'favicon': '/favicon.ico'}
    Swagger(app)
except KeyError as ex:
    raise Exception(f'Environment variable {ex} is missing.')


@app.route("/")
def home():
    return redirect("/apidocs")


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(
        os.path.join(
            app.root_path,
            'assets'),
        'favicon.ico',
        mimetype='image/vnd.microsoft.icon')


app.add_url_rule('/v1/media-url-update-service/update-locator',
                 view_func=MediaUrlUpdateService.as_view('media'))

# If app is started with Flask
if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)-8s [%(filename)s:%(lineno)d] %(message)s')
    app.logger.info('Starting development server')
    app.run(host='0.0.0.0', port=5000, debug=True)
