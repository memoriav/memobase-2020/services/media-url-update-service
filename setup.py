from setuptools import setup

setup(
    name='media_url_update_service_app',
    packages=['media_url_update_service_app'],
    include_package_data=True,
    install_requires=[
        'flask',
        'kafka-python',
        'mysql-connector-python',
        'gunicorn',
        'flasgger'
    ],
)
